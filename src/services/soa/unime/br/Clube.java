package services.soa.unime.br;

public class Clube {
	
	private String nome;
	
	private int vitorias;

	public String getNome() {
		return nome;
	}

	public Clube setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public int getVitorias() {
		return vitorias;
	}

	public Clube setVitorias(int vitorias) {
		this.vitorias = vitorias;
		return this;
	} 

	
}
