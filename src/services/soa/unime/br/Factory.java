package services.soa.unime.br;

import java.util.ArrayList;
import java.util.List;

public class Factory {

	/**
	 * 
	 * @return
	 */
	public static String[] getVictories() {
		List<Clube> clubes = new ArrayList<Clube>();

		clubes.add(new Clube().setNome("Bahia").setVitorias(12));
		clubes.add(new Clube().setNome("S�o Paulo").setVitorias(42));
		clubes.add(new Clube().setNome("Internacional").setVitorias(56));
		clubes.add(new Clube().setNome("Vit�ria").setVitorias(7));
		clubes.add(new Clube().setNome("Crici�ma").setVitorias(12));
		clubes.add(new Clube().setNome("Goais").setVitorias(23));
		clubes.add(new Clube().setNome("Gremio").setVitorias(7));
		clubes.add(new Clube().setNome("Santos").setVitorias(45));
		clubes.add(new Clube().setNome("Corinthians").setVitorias(12));
		clubes.add(new Clube().setNome("Palmeiras").setVitorias(34));
		clubes.add(new Clube().setNome("Sport").setVitorias(6));
		clubes.add(new Clube().setNome("Flamengo").setVitorias(22));
		clubes.add(new Clube().setNome("Fluminense").setVitorias(19));
		clubes.add(new Clube().setNome("Vasco").setVitorias(12));

		String[] array = new String[clubes.size()];
		int index = 0;

		for (Clube value : clubes) {
			array[index] = value.getNome() + '&' + value.getVitorias();
			index++;
		}

		return array;
	}

	/**
	 * 
	 * @return
	 */
	public static String[] getPlayersGoals() {
		List<Jogador> jogadores = new ArrayList<Jogador>();

		jogadores.add(new Jogador().setNome("Pel�")		.setQuantidadeGols(120));
		jogadores.add(new Jogador().setNome("Ronaldo")	.setQuantidadeGols(25));
		jogadores.add(new Jogador().setNome("Valdivia")	.setQuantidadeGols(2));
		jogadores.add(new Jogador().setNome("Neymar")	.setQuantidadeGols(85));
		jogadores.add(new Jogador().setNome("Robinho")	.setQuantidadeGols(12));
		jogadores.add(new Jogador().setNome("Felix")	.setQuantidadeGols(100));
		jogadores.add(new Jogador().setNome("Joao")		.setQuantidadeGols(100));
		jogadores.add(new Jogador().setNome("Diego")	.setQuantidadeGols(45));
		jogadores.add(new Jogador().setNome("Ramires")	.setQuantidadeGols(56));
		jogadores.add(new Jogador().setNome("Ibrahmovic").setQuantidadeGols(42));
		jogadores.add(new Jogador().setNome("Messi")	.setQuantidadeGols(88));
		jogadores.add(new Jogador().setNome("Oscar")	.setQuantidadeGols(4));
		jogadores.add(new Jogador().setNome("Sheik")	.setQuantidadeGols(17));
		jogadores.add(new Jogador().setNome("Fred")		.setQuantidadeGols(55));
		jogadores.add(new Jogador().setNome("Muller")	.setQuantidadeGols(36));
		jogadores.add(new Jogador().setNome("Hernanes")	.setQuantidadeGols(25));
		jogadores.add(new Jogador().setNome("Jobson")	.setQuantidadeGols(6));
		jogadores.add(new Jogador().setNome("Marinho")	.setQuantidadeGols(9));
		jogadores.add(new Jogador().setNome("Gabriel Jesus").setQuantidadeGols(31));

		String[] array = new String[jogadores.size()];
		int index = 0;

		for (Jogador value : jogadores) {
			array[index] = value.getNome() + '&' + value.getQuantidadeGols();
			index++;
		}

		return array;
	}
}
