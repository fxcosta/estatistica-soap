package services.soa.unime.br;

import java.util.List;

public class Estatistica {

	protected Factory factory;

	/**
	 * Simples ola!
	 * @param mensagem
	 * @return
	 */
	public String hello(String nome) {
		return "Ola, mano " + nome;
	}

	/**
	 * Retorna uma lista com os clubes e suas respectivas vitorias no total
	 * @return
	 */
	public String obterClubesVitorias() {
		return this.transformVictoriesXml(Factory.getVictories());
	}
	
	/**
	 * Retorna uma lista com os jogadores e a quantidade de gols marcados no total
	 * @return
	 */
	public String obterJogadoresGols() {
		return this.transformGolsXml(Factory.getPlayersGoals());
	}

	/**
	 * Parseia a lista de clubes que recebemos pra uma string com estrutura xml sem�ntica
	 * @param clubes
	 * @return
	 */
	private String transformVictoriesXml(String[] clubes) {
		String XML = "";
		for (String clube : clubes) {
			
			String[] novoClube = clube.split("&");
			
			XML += "<clube><nome>" + novoClube[0] + "</nome><vitorias>" + novoClube[1] +"</vitorias></clube>";
		}
		XML = "<clubes>" + XML + "</clubes>";
		return XML;
	}
	
	/**
	 * 
	 * @param jogadores
	 * @return
	 */
	private String transformGolsXml(String[] jogadores) {
		String XML = "";
		for (String jogador : jogadores) {
			
			String[] novoJogador = jogador.split("&");
			
			XML += "<jogador><nome>" + novoJogador[0] + "</nome><gols>" + novoJogador[1] +"</gols></jogador>";
		}
		XML = "<jogadores>" + XML + "</jogadores>";
		return XML;
	}
	
	public static void main(String args[]) {
		Estatistica est = new Estatistica();
		System.out.println(est.obterClubesVitorias());
	}
}
