package services.soa.unime.br;

public class Jogador {
	protected String nome;
	
	protected int quantidadeGols;

	public String getNome() {
		return nome;
	}

	public Jogador setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public int getQuantidadeGols() {
		return quantidadeGols;
	}

	public Jogador setQuantidadeGols(int quantidadeGols) {
		this.quantidadeGols = quantidadeGols;
		return this;
	}
	
	
}
